package cn.wanlinus.helloweb.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanli
 * @date 2018-08-08 14:45
 */
@RestController
public class HomeController {

    @GetMapping("/")
    public String helloWorld() {
        return "Hello World";
    }

}
