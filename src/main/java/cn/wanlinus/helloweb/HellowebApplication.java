package cn.wanlinus.helloweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wanli
 */
@SpringBootApplication
public class HellowebApplication {

    public static void main(String[] args) {
        SpringApplication.run(HellowebApplication.class, args);
    }
}
